import {Component, EventEmitter, OnInit} from '@angular/core';
import {DogType} from '../types/DogType';
@Component({
  selector: 'app-dog',
  templateUrl: './dog.component.html',
  styleUrls: ['./dog.component.css']
})
export class DogComponent implements OnInit {
doggies:DogType[];

// lessDogs = (this.doggies.length>1);
// goodNames = ['Abby','Candy','Joe'];

public addDog(eventContent: {name: string, age: number}){
  this.doggies.push({
    name: eventContent.name,
    age: eventContent.age
  })
}

public woof(): void {
  alert('woof');
}

  constructor() {
  }

  ngOnInit() {
    this.doggies = [{name:'dog1',age:2},{name:'dog2',age:3}];
  }
}
