import {Component, Input, OnInit} from '@angular/core';
import {DogType} from '../types/DogType';

@Component({
  selector: 'app-dog-child',
  templateUrl: './dog-child.component.html',
  styleUrls: ['./dog-child.component.css']
})
export class DogChildComponent implements OnInit {

  @Input() dog:DogType;
  constructor() { }

  ngOnInit(): void {
  }

}
