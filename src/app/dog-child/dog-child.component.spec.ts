import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DogChildComponent } from './dog-child.component';

describe('DogChildComponent', () => {
  let component: DogChildComponent;
  let fixture: ComponentFixture<DogChildComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DogChildComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DogChildComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
