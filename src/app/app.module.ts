import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DogComponent } from './dog/dog.component';
import { HeroChildComponent } from './hero-child/hero-child.component';
import {FormsModule} from "@angular/forms";
import { DogChildComponent } from './dog-child/dog-child.component';
import { AddDogComponent } from './add-dog/add-dog.component';
import { CounterComponent } from './counter/counter.component';

@NgModule({
  declarations: [
    AppComponent,
    DogComponent,
    HeroChildComponent,
    DogChildComponent,
    AddDogComponent,
    CounterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
