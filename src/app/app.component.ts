import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'myApp';
  isActive = false;
  items = [];

  public onClick(event: MouseEvent) {
    alert('Hello' + event.clientX);
  }

  ngOnInit(): void {
    this.myInitFunction();
  }

  myInitFunction() {
    this.items = [{name: 'item 1', value: '4'}, {name: 'item 2', value: '5'}]
  }

  addItem(item: NgForm) {
    this.items.push({name: item.value.name, value: item.value.value});
  }

}
