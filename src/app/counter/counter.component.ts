import { Component, OnInit } from '@angular/core';
import {CounterType} from "../types/CounterTypes";

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {

  counterValue: Number;
  counter: CounterType;

  increase(){
    this.counter.increment();
  }

  reset(){
    this.counterValue=0;
    this.counter.reset();
    this.counter.getObservable().subscribe(n => this.counterValue = n);
  }

  getCounter(){
    this.counter.getCounter();
  }

  constructor() {
    this.counterValue = 0;
    this.counter = new CounterType();
    this.counter.getObservable().subscribe(n => this.counterValue = n);
  }

  ngOnInit(): void {
  }

}
