import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero-child',
  templateUrl: './hero-child.component.html',
  styleUrls: ['./hero-child.component.css']
})
export class HeroChildComponent implements OnInit {

  @Input() name: String;
  @Input() value: String;

  constructor() { }

  ngOnInit(): void {
  }

}
